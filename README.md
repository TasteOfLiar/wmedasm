# wmedasm - Wintermute-Engine script disassembler

## Using program not in educational purposes is prohibited!

---

Disassembling Wintermute-Lite binaries not tested try at your own risk 
### Building

First download Go from official site or your linux distributive repository

``
go build -ldflags "-s -w"
``

### Using
``wmedasm -i *your_binary* ``

- **-input** **(-i)** "binary" to specify input file
- **-output** **(-o)** "output_file.wmasm" same but for output (optional)
- **-header** option to dump header of binary
