package main

import (
	"flag"
	"fmt"
	"os"
	"strings"
)

var (
	inputFile   string
	outputFile  string
	printHeader bool
)

const (
	PROGRAM = "wmedasm"
	VERSION = "1.0"
	AUTHOR  = "TasteOfLiar"
)

func parseArgs() {
	flag.StringVar(&inputFile, "input", "", "specify file to disassemble")
	flag.StringVar(&outputFile, "output", "", "specify output file")
	flag.StringVar(&inputFile, "i", "", "specify file to disassemble (shorthand)")
	flag.StringVar(&outputFile, "o", "", "specify output file (shorthand)")
	flag.BoolVar(&printHeader, "header", false, "print file header")

	flag.Parse()
}

func main() {
	parseArgs()

	fmt.Printf("%v v%v by %v (2021)\n\n", PROGRAM, VERSION, AUTHOR)

	if inputFile == "" {
		fmt.Print("no input file\n\n")
		flag.Usage()
		return
	}
	file, err := os.Open(inputFile)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	if outputFile == "" {
		outputFile = strings.ReplaceAll(inputFile, ".script", ".wmasm")
	}
	out, err := os.Create(outputFile)
	if err != nil {
		panic(err)
	}
	defer out.Close()

	d := NewDisassembler(file)
	err = d.Disassemble(out)
	if err != nil {
		panic(err)
	}
	if printHeader {
		fmt.Printf("%+#v", d.exe.Header)
	}
}
