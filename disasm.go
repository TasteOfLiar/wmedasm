package main

import (
	"bufio"
	"encoding/binary"
	"errors"
	"fmt"
	"golang.org/x/text/encoding/charmap"
	"io"
	"os"
)

type ErrUnknownInst struct {
	Inst   Instruction
	Offset int64
}

func (e *ErrUnknownInst) Error() string {
	return fmt.Sprintf("unknown instruction (%v) at %+#x",
		e.Inst.Opcode,
		e.Inst.Offset)
}

type ErrWrongParam struct {
	Param  interface{}
	Offset int64
}

func (e *ErrWrongParam) Error() string {
	return fmt.Sprintf("Wrong param (%v) at %+#x",
		e.Param,
		e.Offset)
}

var (
	ErrNoTable = errors.New("table does not exists")
	ErrMagic   = errors.New("magic not watch")
)

var WmeScriptMagic = []byte{0xDE, 0xAD, 0xC0, 0xDE}

type Disassembler struct {
	file *os.File
	exe  *Executable
}

func NewDisassembler(file *os.File) *Disassembler {
	return &Disassembler{
		file: file,
		exe:  &Executable{},
	}
}

func (d *Disassembler) Disassemble(w io.Writer) error {
	err := d.disassemble()
	if err != nil {
		return err
	}
	ag := assemblyGenerator{Codes: d.exe.Code}
	err = ag.Generate(w)
	if err != nil {
		return err
	}

	return nil
}

func (d *Disassembler) disassemble() error {
	err := d.readHeader()
	if err != nil {
		return err
	}
	if !d.checkMagic() {
		return ErrMagic
	}

	err = d.readFunctionTable()
	if err != nil && err != ErrNoTable {
		return err
	}
	err = d.readStringTable()
	if err != nil && err != ErrNoTable {
		return err
	}
	err = d.readEventTable()
	if err != nil && err != ErrNoTable {
		return err
	}
	err = d.readDLLTable()
	if err != nil && err != ErrNoTable {
		return err
	}
	err = d.readMethodTable()
	if err != nil && err != ErrNoTable {
		return err
	}
	err = d.readCode()
	if err != nil && err != ErrNoTable {
		return err
	}

	return nil
}

func (d *Disassembler) checkMagic() bool {
	return d.exe.Header.Magic == binary.LittleEndian.Uint32(WmeScriptMagic)
}

func (d *Disassembler) readUint() (uint32, error) {
	num := make([]byte, 4)
	_, err := d.file.Read(num)
	if err != nil {
		return 0, err
	}

	return binary.LittleEndian.Uint32(num), nil
}

func (d *Disassembler) readInt() (int32, error) {
	num, err := d.readUint()
	return int32(num), err
}

func (d *Disassembler) readFloat() (float64, error) {
	var result float64
	err := binary.Read(d.file, binary.LittleEndian, &result)
	if err != nil {
		return 0, err
	}

	return result, nil
}

func (d *Disassembler) readString() (string, error) {
	offset, err := d.file.Seek(0, io.SeekCurrent)
	if err != nil {
		return "", err
	}
	//fmt.Printf("current offset %#x\n", offset)
	rawstr, err := bufio.NewReader(d.file).ReadBytes('\x00')
	if err != nil {
		return "", err
	}

	decoder := charmap.Windows1251.NewDecoder()
	//Декодируем сырую строку из win1251 в UTF-8 без нуль-терминатора
	bytesstr, err := decoder.Bytes(rawstr[:len(rawstr)-1])
	if err != nil {
		return "", err
	}

	//bufio.Reader передвигает указатель слишком далеко. Возвращаем на длину строки + нуль-терминатор
	_, err = d.file.Seek(offset+int64(len(rawstr)), io.SeekStart)
	if err != nil {
		return "", err
	}

	/*newoffset, _ := d.file.Seek(0, io.SeekCurrent)
	fmt.Printf("new offset %#x\n", newoffset)
	*/
	return string(bytesstr), nil
}

func (d *Disassembler) nameByMethodId(id uint32) string {
	for _, m := range d.exe.MethodTable.Methods {
		if m.Id == id {
			return m.Name
		}
	}

	return ""
}

func (d *Disassembler) nameByFunctionId(id uint32) string {
	for _, m := range d.exe.FunctionTable.Functions {
		if m.Id == id {
			return m.Name
		}
	}

	return ""
}

func (d *Disassembler) nameByEventId(id uint32) string {
	for _, m := range d.exe.EventTable.Events {
		if m.Id == id {
			return m.Name
		}
	}

	return ""
}

func (d *Disassembler) readInst() (*Instruction, error) {
	offset, err := d.file.Seek(0, io.SeekCurrent)
	if err != nil {
		return nil, err
	}
	op, err := d.readUint()
	if err != nil {
		return nil, err
	}

	inst := Instruction{}
	inst.Opcode = Opcode(op)
	inst.Offset = uint32(offset)

	switch Opcode(op) {
	case II_PUSH_FLOAT:
		inst.Param, err = d.readFloat()
		if err != nil {
			return nil, err
		}
	case II_JMP, II_JMP_FALSE:
		inst.Param, err = d.readUint()
		if err != nil {
			return nil, err
		}
	case II_DBG_LINE, II_PUSH_INT, II_PUSH_BOOL, II_CORRECT_STACK:
		inst.Param, err = d.readInt()
		if err != nil {
			return nil, err
		}
	case II_PUSH_VAR_REF, II_POP_VAR, II_PUSH_VAR, II_PUSH_THIS,
		II_EXTERNAL_CALL, II_DEF_VAR, II_DEF_CONST_VAR, II_DEF_GLOB_VAR:
		strnum, err := d.readUint()
		if err != nil {
			return nil, err
		}
		if strnum <= d.exe.StringTable.StringCount {
			inst.Param = d.exe.StringTable.Strings[strnum]
		} else {
			return nil, &ErrWrongParam{strnum, offset}
		}
	case II_CALL:
		id, err := d.readUint()
		if err != nil {
			return nil, err
		}
		name := d.nameByFunctionId(id)
		if name != "" {
			inst.Param = name
			break
		}
		name = d.nameByMethodId(id)
		if name != "" {
			inst.Param = name
			break
		}
		return nil, &ErrWrongParam{id, offset}
	case II_SCOPE:
		name := d.nameByFunctionId(uint32(offset))
		if name != "" {
			inst.Param = fmt.Sprintf("FUNCTION \"%v\"", name)
			break
		}
		name = d.nameByMethodId(uint32(offset))
		if name != "" {
			inst.Param = fmt.Sprintf("METHOD \"%v\"", name)
			break
		}
		name = d.nameByEventId(uint32(offset))
		if name != "" {
			inst.Param = fmt.Sprintf("EVENT \"%v\"", name)
			break
		}
		return nil, &ErrWrongParam{offset, offset}
	case II_PUSH_STRING:
		str, err := d.readString()
		if err != nil {
			return nil, err
		}
		inst.Param = str
	default:
		if len(OpcodeToString) >= int(inst.Opcode) {
			inst.Param = nil
		} else {
			return nil, &ErrUnknownInst{Inst: inst, Offset: offset}
		}
	}

	return &inst, nil
}

func (d *Disassembler) readHeader() error {
	var err error
	_, err = d.file.Seek(0, io.SeekStart)
	if err != nil {
		return err
	}

	d.exe.Header.Magic, err = d.readUint()
	if err != nil {
		return err
	}
	d.exe.Header.Version, err = d.readUint()
	if err != nil {
		return err
	}
	d.exe.Header.CodeOffset, err = d.readUint()
	if err != nil {
		return err
	}
	d.exe.Header.FunctionTableOffset, err = d.readUint()
	if err != nil {
		return err
	}
	d.exe.Header.StringTableOffset, err = d.readUint()
	if err != nil {
		return err
	}
	d.exe.Header.EventTableOffset, err = d.readUint()
	if err != nil {
		return err
	}
	d.exe.Header.DLLTableOffset, err = d.readUint()
	if err != nil {
		return err
	}
	d.exe.Header.MethodTableOffset, err = d.readUint()
	if err != nil {
		return err
	}
	d.exe.Header.SourceName, err = d.readString()
	if err != nil {
		return err
	}

	return nil
}

func (d *Disassembler) readFunctionTable() error {
	var err error
	_, err = d.file.Seek(int64(d.exe.Header.FunctionTableOffset), io.SeekStart)
	if err != nil {
		return err
	}

	d.exe.FunctionTable.FunctionCount, err = d.readUint()
	if err != nil {
		return err
	}

	if d.exe.FunctionTable.FunctionCount == 0 {
		return ErrNoTable
	}

	for i := uint32(0); i < d.exe.FunctionTable.FunctionCount; i++ {
		f := Function{}

		f.Id, err = d.readUint()
		if err != nil {
			return err
		}
		f.Name, err = d.readString()
		if err != nil {
			return err
		}

		d.exe.FunctionTable.Functions = append(d.exe.FunctionTable.Functions, f)
	}

	return nil
}

func (d *Disassembler) readStringTable() error {
	var err error
	_, err = d.file.Seek(int64(d.exe.Header.StringTableOffset), io.SeekStart)
	if err != nil {
		return err
	}

	d.exe.StringTable.StringCount, err = d.readUint()
	if err != nil {
		return err
	}
	if d.exe.StringTable.StringCount == 0 {
		return ErrNoTable
	}
	for i := uint32(0); i < d.exe.StringTable.StringCount; i++ {
		_, err = d.readUint()
		if err != nil {
			return err
		}
		str, err := d.readString()
		if err != nil {
			return err
		}

		d.exe.StringTable.Strings = append(d.exe.StringTable.Strings, str)
	}

	return nil
}

func (d *Disassembler) readEventTable() error {
	var err error
	_, err = d.file.Seek(int64(d.exe.Header.EventTableOffset), io.SeekStart)
	if err != nil {
		return err
	}

	d.exe.EventTable.EventCount, err = d.readUint()
	if err != nil {
		return err
	}

	if d.exe.EventTable.EventCount == 0 {
		return ErrNoTable
	}

	for i := uint32(0); i < d.exe.EventTable.EventCount; i++ {
		f := Event{}

		f.Id, err = d.readUint()
		if err != nil {
			return err
		}
		f.Name, err = d.readString()
		if err != nil {
			return err
		}

		d.exe.EventTable.Events = append(d.exe.EventTable.Events, f)
	}

	return nil
}

func (d *Disassembler) readDLLTable() error {
	var err error
	_, err = d.file.Seek(int64(d.exe.Header.DLLTableOffset), io.SeekStart)
	if err != nil {
		return err
	}

	d.exe.DLLTable.DLLCount, err = d.readUint()
	if err != nil {
		return err
	}

	if d.exe.DLLTable.DLLCount == 0 {
		return ErrNoTable
	}

	for i := uint32(0); i < d.exe.DLLTable.DLLCount; i++ {
		dll := DLL{}
		dll.DllName, err = d.readString()
		if err != nil {
			return err
		}

		dll.Name, err = d.readString()
		if err != nil {
			return err
		}

		ct, err := d.readUint()
		if err != nil {
			return err
		}
		dll.CallType = CallType(ct)

		ret, err := d.readUint()
		if err != nil {
			return err
		}
		dll.Returns = ExternalType(ret)

		dll.ParamCount, err = d.readUint()
		if err != nil {
			return err
		}

		for i := uint32(0); i < dll.ParamCount; i++ {
			param, err := d.readUint()
			if err != nil {
				return err
			}
			dll.Params = append(dll.Params, ExternalType(param))
		}

		d.exe.DLLTable.DlLs = append(d.exe.DLLTable.DlLs, dll)
	}

	return nil
}

func (d *Disassembler) readMethodTable() error {
	var err error
	_, err = d.file.Seek(int64(d.exe.Header.MethodTableOffset), io.SeekStart)
	if err != nil {
		return err
	}

	d.exe.MethodTable.MethodCount, err = d.readUint()
	if err != nil {
		return err
	}

	if d.exe.MethodTable.MethodCount == 0 {
		return ErrNoTable
	}

	for i := uint32(0); i < d.exe.MethodTable.MethodCount; i++ {
		m := Method{}

		m.Id, err = d.readUint()
		if err != nil {
			return err
		}
		m.Name, err = d.readString()
		if err != nil {
			return err
		}

		d.exe.MethodTable.Methods = append(d.exe.MethodTable.Methods, m)
	}

	return nil
}

func (d *Disassembler) readCode() error {
	var err error
	_, err = d.file.Seek(int64(d.exe.Header.CodeOffset), io.SeekStart)
	if err != nil {
		return err
	}

	ptr := int64(d.exe.Header.CodeOffset)
	for ptr < int64(d.exe.Header.FunctionTableOffset) {
		inst, err := d.readInst()
		if err != nil {
			return err
		}
		ptr, err = d.file.Seek(0, io.SeekCurrent)
		d.exe.Code = append(d.exe.Code, *inst)
	}

	return nil
}

type assemblyGenerator struct {
	Codes      []Instruction
	jmpOffsets map[uint32]string //Мапа с адресом в качестве ключа и названием метки в значении
}

func (a *assemblyGenerator) Generate(w io.Writer) error {
	if a.jmpOffsets == nil {
		a.jmpOffsets = make(map[uint32]string)
	}

	_, err := fmt.Fprintf(w, "; Code generated by %v\n\n", PROGRAM)
	if err != nil {
		return err
	}

	//Первый проход для сбора адресов для перехода
	for _, inst := range a.Codes {
		switch inst.Opcode {
		case II_JMP, II_JMP_FALSE:
			label := fmt.Sprintf("LAB_%X", inst.Param.(uint32))
			a.jmpOffsets[inst.Param.(uint32)] = label
		}
	}

	for _, inst := range a.Codes {
		op := OpcodeToString[inst.Opcode]
		var param interface{}

		if inst.Param == nil {
			param = ""
		} else {
			param = inst.Param
		}

		switch inst.Opcode {
		case II_JMP, II_JMP_FALSE:
			param = a.jmpOffsets[inst.Param.(uint32)]
		case II_PUSH_STRING:
			param = fmt.Sprintf("\"%v\"", inst.Param)
		}

		if label, ok := a.jmpOffsets[inst.Offset]; ok {
			_, err := fmt.Fprintf(w, ":%v ", label)
			if err != nil {
				return err
			}
		}

		_, err := fmt.Fprintf(w, "%v %v\n", op, param)
		if err != nil {
			return err
		}
	}

	return nil
}
