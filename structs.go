package main

import "encoding/binary"

/*
DWORD magic
DWORD version
DWORD code_start
DWORD function_table_start
DWORD string_table_start
DWORD event_table_start
0x0101: DWORD dll_table_start
0x0102: DWORD method_table_start
char[?] source_name
  code
  function table (num,  pos, name...)
  string table (num,  id, name...)
  event table (num,  id, name...)
  0x0101: dll table (num,  dll, name, call, returns, num_params, param1, param2...)
  0x0102: method table (num,  pos, name...)

*/

type Header struct {
	Magic               uint32
	Version             uint32
	CodeOffset          uint32
	FunctionTableOffset uint32
	StringTableOffset   uint32
	EventTableOffset    uint32
	DLLTableOffset      uint32
	MethodTableOffset   uint32
	SourceName          string
}

type Function struct {
	Id   uint32
	Name string
}

type FunctionTable struct {
	FunctionCount uint32
	Functions     []Function
}

type StringTable struct {
	StringCount uint32
	Strings     []string
}

type Event struct {
	Id   uint32
	Name string
}

type EventTable struct {
	EventCount uint32
	Events     []Event
}

type DLL struct {
	DllName  string
	Name     string
	CallType CallType
	Returns  ExternalType

	ParamCount uint32
	Params     []ExternalType
}

type DLLTable struct {
	DLLCount uint32
	DlLs     []DLL
}

type Method struct {
	Id   uint32
	Name string
}

type MethodTable struct {
	MethodCount uint32
	Methods     []Method
}

type Instruction struct {
	Offset uint32
	Opcode Opcode
	Param  interface{}
}

func (i *Instruction) Size() int {
	size := binary.Size(i.Opcode)
	switch i.Param.(type) {
	case string:
		size += len(i.Param.(string)) + 1
	case nil:
	default:
		s := binary.Size(i.Param)
		if s < 0 {
			size = s
		} else {
			size += s
		}
	}

	return size
}

type Executable struct {
	Header        Header
	Code          []Instruction //Временное решение, возможно я придумаю что лучше
	FunctionTable FunctionTable
	StringTable   StringTable
	EventTable    EventTable
	DLLTable      DLLTable
	MethodTable   MethodTable
}
